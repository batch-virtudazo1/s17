/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function myInfo(){
		let fullName = prompt("Please enter your fullname: ");
		let age = prompt("Please enter your age: ");
		let currentLocation = prompt("Please enter your current lcoation: ");

		console.log("Hi " + fullName);
		console.log(fullName + '\'s is ' + age);
		console.log(fullName + " is located at " + currentLocation);
	}

	myInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function myTop5Artists(){
		let myTop5 = ['Shirebound and Bucking', 'Nobita', 'Sidewalk Prophets', 'TEEKS', 'Ben and Ben'];
		console.log(myTop5);
	}

	myTop5Artists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myTop5Movies(){
		let top1Movie = "Avengers: Endgame";
		let top2Movie = "Us";
		let top3Movie = "Toy Story 4";
		let top4Movie = "Knives Out";
		let top5Movie = "The Farewell";

		let top1MovieRating = "94%";
		let top2MovieRating = "93%";
		let top3MovieRating = "97%";
		let top4MovieRating = "97%";
		let top5MovieRating = "97%";

		console.log("1. " + top1Movie + '\n');
		console.log("Tomatometer for " + top1Movie + " : " + top1MovieRating + '\n');
		console.log("2. " + top2Movie + '\n');
		console.log("Tomatometer for " +  top2Movie + " : " + top2MovieRating + '\n');
		console.log("3. " + top3Movie + '\n');
		console.log("Tomatometer for " +  top3Movie + " : " + top3MovieRating + '\n');
		console.log("4. " + top4Movie + '\n');
		console.log("Tomatometer for " +  top4Movie + " : " + top4MovieRating + '\n');
		console.log("5. " + top5Movie + '\n');
		console.log("Tomatometer for " +  top5Movie + " : " + top5MovieRating + '\n');

    };
	myTop5Movies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);